<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="web.CreditModel" %>
<% 
 CreditModel model=(CreditModel)request.getAttribute("creditModel");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Simulateur de Credit </title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
</head>
<body>
    <div>
	  <form action="calculerMensualite.do" method="post">
	     <table class="table table-striped">
	        <tr>
	           <td>Montant :</td>
	           <td><input type="text" name ="montant" value="<%= model.getMontant() %>"/></td>
	           <td>$</td>
	        </tr>
	        <tr>
	           <td>Taux:</td>
	           <td><input type="text" name ="taux" value="<%=model.getTaux()%>" /></td>
	           <td>%</td>
	         <tr>
	           <td>Duree :</td>
	           <td><input type="text" name ="duree"value="<%= model.getDuree() %>" /></td>
	           <td>mois</td>
	         </tr>
	     </table>
	     <button class="btn btn-success" type="submit">Calculer</button>
	  </form>
    </div>
    <div>
     <p></p>
     Mensualite = <%=model.getMensualite() %>
    </div>
</body>
</html>