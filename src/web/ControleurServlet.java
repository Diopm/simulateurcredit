package web;

import java.io.IOException;
import java.rmi.ServerException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import metier.CreditMetierImpl;

/**
 * Servlet implementation class ConstructeurServlet
 */
@WebServlet(name="cs",urlPatterns = { "/ContoleurServlet","*.do"})
public class ControleurServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private CreditMetierImpl metier;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControleurServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void init() throws ServletException{
    	
    	metier = new CreditMetierImpl();
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		request.setAttribute("creditModel", new CreditModel());
		request.getRequestDispatcher("vueCredit.jsp").forward(request, response);
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//recuperer les donnees
		double montant = Double.parseDouble(request.getParameter("montant"));
		double taux  = Double.parseDouble(request.getParameter("taux"));
		int duree = Integer.parseInt(request.getParameter("duree"));
		
		//Stocker les donnees dans le modele
		CreditModel model = new CreditModel();
		model.setMontant(montant);
		model.setTaux(taux);
		model.setDuree(duree);
		
		// Faire appel a la couche metier pour effectuer les traitements
		double mensualite = metier.calculerMensualiteCredit(montant, taux, duree);
		//stocker le resultat dans le modele 
		model.setMensualite(mensualite);
		
		//Stocker le modele dans request
		request.setAttribute("creditModel", model);
		// faire un forward vers la vue jsp
		request.getRequestDispatcher("vueCredit.jsp").forward(request, response);
		
		
	}

}
